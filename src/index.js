const Grabber = require("./grabber");
const CodeGen = require("./codeGen");

module.exports = { Grabber, CodeGen };
