const { TS } = require('@avstantso/node-js--code-gen');
const Abort = require('./abort');
const parseParams = require('./parseParams');

const funcGenTypeDecl = `T extends Func.ReturnType = Func.ReturnType`;

function typeDef(type) {
  return 'any' === type ? 'Arg' : `Arg<${type}>`;
}

function paramsDef(params) {
  return params.map(({ isRest, isOptional, name, type }) =>
    isRest
      ? `...rest: ${typeDef(type)}[]`
      : `${name}${isOptional ? '?' : ''}: ${typeDef(type)}`
  );
}

function paramsSet(params) {
  return params.map(({ isRest, name }) => (isRest ? `...rest` : `${name}`));
}

function funcName(name) {
  return `${name}.name`;
}

function makeSimpleFunc(name, paramsStr, silent) {
  try {
    const params = parseParams(name, paramsStr, silent);

    return TS.Export.Function(
      name,
      funcGenTypeDecl,
      paramsDef(params).join(','),
      '',
      TS.Return(
        TS.Call.Gen('Func.raw', 'T', [funcName(name), ...paramsSet(params)])
      ) + ';'
    );
  } catch (e) {
    if (e instanceof Abort) return '';

    e.context = { ...(e.context || {}), name, paramsStr };

    throw e;
  }
}

function makeOverloadFunc(name, overloadsStr, silent) {
  try {
    const overloads = overloadsStr.map((paramsStr) =>
      parseParams(name, paramsStr, silent)
    );

    return (
      TS.Export.Namespace(
        name,
        TS.Export.Type(
          'Overload',
          TS.Object(
            overloads
              .map((params) =>
                TS.Type.Field(
                  `<${funcGenTypeDecl}> (${paramsDef(params).join(',')})`,
                  'Func<T>'
                )
              )
              .join('')
          )
        )
      ) +
      TS.Export.Const(
        name,
        `${name}.Overload`,
        TS.ArrowFunc.Short(
          funcGenTypeDecl,
          '...rest: Arg[]',
          '',
          TS.Call.Gen('Func.raw', 'T', [funcName(name), '...rest'])
        )
      )
    );
  } catch (e) {
    if (e instanceof Abort) return '';

    e.context = { ...(e.context || {}), name, overloadsStr };

    throw e;
  }
}

module.exports = { makeSimpleFunc, makeOverloadFunc };
