class Abort extends Error {
  constructor(message) {
    super(message);

    Object.setPrototypeOf(this, Abort.prototype);
  }
}

module.exports = Abort;
