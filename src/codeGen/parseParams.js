const Names = require('./argsNames');
const Abort = require('./abort');

module.exports = (funcName, paramsStr, silent) => {
  let r = [];
  let p = '';
  let isOptionalDepth = 0;

  const add = (name, type) => {
    const cur = {
      name,
      type,
      ...(isOptionalDepth > 0 ? { isOptional: true } : {}),
    };
    r.push(cur);
  };

  const addRest = () => r.push({ isRest: true, type: r[r.length - 1].type });

  const processParam = () => {
    if (!p) return;

    p = p.trim();
    // fix generic <T> conflict
    if ('T' === p) p = 'T1';

    if (Names.date.includes(p)) return add(p, 'Date');

    if (Names.number.includes(p)) return add(p, 'number');

    if (/^str\d?$/.test(p) || Names.string.includes(p)) return add(p, 'string');

    if (/^\[?\w+$/.test(p)) {
      if ('[' === p[0]) p = p.substring(1, p.length - 1);
      return add(p, 'any');
    }

    if ('...' === p) return addRest();

    !silent &&
      console.log(
        `\x1b[33mAbort\x1b[0m parseParams for \x1b[32m${funcName}\x1b[0m: %O`,
        {
          ...(r.length ? { params: r } : {}),
          mismatch: p,
        }
      );

    throw new Abort(p);
  };

  for (let i = 0; i < paramsStr.length; i++) {
    if ('[' === paramsStr[i]) {
      isOptionalDepth++;
      continue;
    }

    if (']' === paramsStr[i]) {
      processParam();
      p = '';
      continue;
    }

    if (',' === paramsStr[i]) {
      processParam();
      p = '';
      continue;
    }

    p += paramsStr[i];
  }
  processParam();

  return r.map((p, index) => {
    const i = r.findIndex((p0) => p0.name === p.name);
    if (i < index) return { ...p, name: `${p.name}${i + 1}` };
    return p;
  });
};
