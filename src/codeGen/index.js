const path = require('path');
const fs = require('fs');
const Promise = require('bluebird');
const { writeSourceFile, TS } = require('@avstantso/node-js--code-gen');
const Grabber = require('../grabber');
const { makeSimpleFunc, makeOverloadFunc } = require('./makeFuncs');

const sourceImports =
  TS.Import.Members('Func, Arg').From('@avstantso/node-js--my-sql-wrapper') +
  '\r\n\r\n';

const nameRegExp = new RegExp(/\w+/);

function CodeGen(
  options = {
    pages: [
      'string-functions',
      'miscellaneous-functions',
      'date-and-time-functions',
    ],
    cachePath: undefined,
    manually: { filename: undefined },
    silent: false,
  }
) {
  const { pages, cachePath, manually, silent } = options;
  grabber = Grabber({ cachePath, silent });

  async function generate(folder, scriptName) {
    if (!fs.existsSync(folder)) fs.mkdirSync(folder);

    let indexSource = '';
    const toIndexExport = [];

    await Promise.mapSeries(pages, async (pageName) => {
      let source = sourceImports;
      const toPageExport = [];

      const { items } = await grabber.functionsPage(pageName);

      items.forEach((item) => {
        const [name] = nameRegExp.exec(item);

        const overloads = (() => {
          let o = item.split(name);
          o.splice(0, 1);
          o = o.filter((oo) => ',' !== oo);
          o = o.map((oo) => oo.replace(/,$/, ''));
          o = o.map((oo) => oo.substring(1, oo.length - 1));
          return o;
        })();

        const code =
          1 >= overloads.length
            ? makeSimpleFunc(name, overloads.length ? overloads[0] : '', silent)
            : makeOverloadFunc(name, overloads, silent);
        if (code) {
          toPageExport.push(name);
          source += code;
        }
      });

      await writeSourceFile(
        path.resolve(folder, `${pageName}.ts`),
        source,
        scriptName,
        { silent }
      );

      toIndexExport.push(TS.Export.Asterisk.From(`./${pageName}`));
    });

    if (manually)
      toIndexExport.push(TS.Export.Asterisk.From(manually.filename));

    indexSource += '\r\n\r\n' + toIndexExport.join('');

    await writeSourceFile(
      path.resolve(folder, `index.ts`),
      indexSource,
      scriptName,
      { silent }
    );
  }

  return { generate };
}

module.exports = CodeGen;
