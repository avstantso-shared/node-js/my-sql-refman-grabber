const getDescendantByTag = (node, tag, className = undefined) => {
  for (let i = 0; i < node.childNodes?.length; i++) {
    if (node.childNodes[i].tagName === tag) {
      if (
        !className ||
        node.childNodes[i].attrs.find(
          (a) => a.name === 'class' && a.value.split(' ').includes(className)
        )
      ) {
        return node.childNodes[i];
      }
    }

    const result = getDescendantByTag(node.childNodes[i], tag, className);
    if (result) return result;
  }

  return null;
};

const tagsList = (node, tag) => {
  const list = [];

  const walker = (node) => {
    for (let i = 0; i < node.childNodes?.length; i++) {
      if (node.childNodes[i].tagName === tag) list.push(node.childNodes[i]);
      else walker(node.childNodes[i]);
    }
  };

  walker(node);

  return list;
};

const nodeText = (node) => {
  let text = '';

  const walker = (node) => {
    for (let i = 0; i < node.childNodes?.length; i++) {
      if ('#text' === node.childNodes[i].nodeName)
        text += node.childNodes[i].value.trim();

      walker(node.childNodes[i]);
    }
  };

  walker(node);

  return text.replace(/\s+/g, ' ').replace(/\s+/g, ' ');
};

module.exports = { getDescendantByTag, tagsList, nodeText };
