const path = require('path');
const fs = require('fs');

module.exports = ({ cachePath, silent }) => {
  const toCache = (filename, data) => {
    if (!data) {
      console.log('nothing for cache!');
      return;
    }

    if (!fs.existsSync(cachePath)) fs.mkdirSync(cachePath);

    try {
      const cacheFilename = path.resolve(cachePath, filename);

      fs.writeFileSync(
        cacheFilename,
        'string' === typeof data ? data : JSON.stringify(data)
      );

      !silent && console.log(`\x1b[36m+\x1b[0m ${cacheFilename}`);
    } catch (e) {
      console.log('%O', data);
      throw e;
    }
  };

  const fromCache = (filename) =>
    (fs.existsSync(path.resolve(cachePath, filename)) &&
      JSON.parse(fs.readFileSync(path.resolve(cachePath, filename)))) ||
    undefined;

  return { toCache, fromCache };
};
