const axios = require('axios');
const { parse } = require('parse5');
const { getDescendantByTag, tagsList, nodeText } = require('./parsing');
const Cache = require('./cache');

const ai = axios.create({
  baseURL: 'https://dev.mysql.com/doc/refman/8.0/en/',
  timeout: 10000,
  headers: {},
});

const Grabber = ({ cachePath, silent }) => {
  const cache = !!cachePath;
  const { toCache, fromCache } = Cache({ cachePath, silent });

  const functionsToc = async () => {
    const cacheFile = 'functionsToc.json';

    if (cache) {
      const data = fromCache(cacheFile);
      if (data?.length) return data;
    }

    return ai
      .get('functions.html')
      .then((r) => r.data)
      .then((r) => parse(r))
      .then((r) => getDescendantByTag(r, 'div', 'toc'))
      .then((r) => tagsList(r, 'a'))
      .then((r) =>
        r.map(({ attrs }) => attrs.find((a) => 'href' === a.name)?.value)
      )
      .then((r) => {
        toCache(cacheFile, r);
        return r;
      });
  };

  const functionsPage = async (name) => {
    const cacheFile = `${name}.json`;

    if (cache) {
      const data = fromCache(cacheFile);
      if (data) return data;
    }

    return ai
      .get(`${name}.html`)
      .then((r) => r.data)
      .then((r) => parse(r))
      .then(async (r) => {
        const toc = await Promise.resolve()
          .then(() => getDescendantByTag(r, 'div', 'table-contents'))
          .then((r) => tagsList(r, 'a'))
          .then((r) =>
            r.map(({ attrs }) => attrs.find((a) => 'href' === a.name)?.value)
          )
          .then((r) => r.map((i) => i.split('#')[1]));

        const items = await Promise.resolve()
          .then(() => tagsList(r, 'a'))
          .then((r) =>
            r.filter(({ attrs }) =>
              toc.includes(attrs.find((a) => 'name' === a.name)?.value)
            )
          )
          .then((r) => r.map((i) => getDescendantByTag(i.parentNode, 'p')))
          .then((r) => r.map((i) => nodeText(i)));
        return { toc, items };
      })
      .then((r) => {
        toCache(cacheFile, r);
        return r;
      });
  };

  return { functionsToc, functionsPage };
};

module.exports = Grabber;
